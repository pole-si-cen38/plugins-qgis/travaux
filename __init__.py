# -*- coding: utf-8 -*-
"""
/***************************************************************************
 travaux
                                 A QGIS plugin
 travaux
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2022-08-22
        copyright            : (C) 2022 by colas
        email                : colas.geier@cen-isere.org
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load travaux class from file travaux.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .travaux import travaux
    return travaux(iface)
