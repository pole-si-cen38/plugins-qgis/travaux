# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=travaux
qgisMinimumVersion=3.0
description=This plugin allow you to manage roadworks of CEN 38
version=0.1
author=Colas Geier / CEN 38
email=colas.geier@cen-isere.org

about=Ce plugin est utilisable UNIQUEMENT par le responsable travaux. Lui seul est en droit de modifier la base de données "travaux". Contactez la team SIG en cas de besoin. Pour activer l'utilisation de ce plugin, vous avez besoin de charger au moins une des couches de référence de la base de données travaux (cr_XXX_saisie_travaux).

tracker=http://bugs
repository=http://repo
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=python,gestion,travaux,cen38

homepage=http://homepage
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=psycopg2

Category of the plugin: Raster, Vector, Database or Web
# category=Database

# If the plugin can run on QGIS Server.
server=False

